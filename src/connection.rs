use crate::packets::{AdminUpdateTypeId, PollFrequency};
use crate::types;

use super::packets;
use anyhow::anyhow;
use bincode::enc::write::Writer;
use state::Handshake;
use std::collections::HashMap;
use std::fmt::{Debug, Display};
use std::io::Write;
use std::net::{SocketAddr, TcpStream};

#[derive(Debug)]
pub struct PollInfoTypeSettings {
    pub frequency: packets::PollFrequency,
    pub poll_type: packets::AdminUpdateTypeId,
}

#[derive(Debug, Default)]
pub struct PollInfo {
    pub updates: HashMap<packets::AdminUpdateTypeId, PollFrequency>,
}

mod state {
    use std::{collections::HashMap, mem};

    use anyhow::{anyhow, Context};

    use crate::packets;

    use super::PollInfo;
    #[derive(Debug)]
    pub struct ReadyToPoll {}

    impl ReadyToPoll {
        pub fn new() -> Self {
            Self {}
        }
    }
    #[derive(Default, Debug)]
    pub struct HandshakeResult {
        pub poll: PollInfo,
        pub protocol_version: u8,
        pub server_name: String,
        pub revision: String,
        pub is_dedicated: bool,
        pub seed: u32,
        pub is_landscape_mod: bool,
        pub date: u32,
        pub size_x: u16,
        pub size_y: u16,
    }
    #[derive(PartialEq, Eq, Debug)]

    enum HandshakeCounter {
        Start,
        SentAuth,
        GotProtocol,
        GotWelcome,
    }
    #[derive(Debug)]
    pub struct Handshake {
        pub handshake_results: HandshakeResult,
        user: String,
        password: String,
        counter: HandshakeCounter,
    }

    impl Handshake {
        pub fn new(user: String, password: String) -> Self {
            Self {
                user: user,
                password: password,
                handshake_results: Default::default(),
                counter: HandshakeCounter::Start,
            }
        }

        pub fn send<W: std::io::Write>(&mut self, writer: &mut W) -> anyhow::Result<()> {
            if self.counter == HandshakeCounter::Start {
                let res = bincode::encode_to_vec(
                    packets::AdminJoin::new(&self.user, &self.password),
                    bincode::config::standard().with_fixed_int_encoding(),
                )?;
                println!("{:?}", res);
                writer.write_all(&res).context("Failed to send to socket")?;
                self.counter = HandshakeCounter::SentAuth;
                Ok(())
            } else {
                Err(anyhow!("Called send in invalid state {:?}", self.counter))
            }
        }
        unsafe fn convert_updates(
            updates: &[packets::AdminUpdateTypeState; 10],
        ) -> HashMap<packets::AdminUpdateTypeId, packets::PollFrequency> {
            let mut map = HashMap::<packets::AdminUpdateTypeId, packets::PollFrequency>::new();
            for (index, update) in updates.iter().enumerate() {
                map.insert(
                    mem::transmute(update.id as u8),
                    mem::transmute(update.frequency as u8),
                );
            }

            map
        }
        pub fn handle_packet(&mut self, packet: &packets::Packet) -> anyhow::Result<bool> {
            if self.counter == HandshakeCounter::SentAuth {
                match &packet {
                    packets::Packet::Protocol(p) => {
                        self.handshake_results.protocol_version = p.protocol_version;
                        self.handshake_results.poll.updates =
                            unsafe { Self::convert_updates(&p.updates) };
                        Ok(false)
                    }
                    _ => Err(anyhow::anyhow!(
                        "Received invalid packet {:?} in state {:?}",
                        packet,
                        self.counter
                    )),
                }
            } else if self.counter == HandshakeCounter::GotProtocol {
                match &packet {
                    packets::Packet::Welcome(p) => {
                        self.handshake_results.date = p.date;
                        self.handshake_results.is_dedicated = p.is_dedicated == 1;
                        self.handshake_results.seed = p.seed;
                        self.handshake_results.revision = p.revision.clone().try_into()?;
                        self.handshake_results.server_name = p.name.clone().try_into()?;
                        self.handshake_results.size_x = p.size_x;
                        self.handshake_results.size_y = p.size_y;
                        self.handshake_results.is_landscape_mod = p.is_landscape_mod == 1;
                        Ok(true)
                    }
                    _ => Err(anyhow::anyhow!(
                        "Received invalid packet {:?} in state {:?}",
                        packet,
                        self.counter
                    )),
                }
            } else {
                Err(anyhow!("Invalid packet {:?}", packet))
            }
        }
    }
}

#[derive(Debug)]
enum States {
    None,
    Handshake(state::Handshake),
    ReadyToPoll(state::ReadyToPoll),
}

pub struct TTDConnection {
    pub socket: TcpStream,
    pub connection_metadata: Option<state::HandshakeResult>,
    pub state: States,
}
impl TTDConnection {
    pub fn new(addres: SocketAddr) -> anyhow::Result<Self> {
        if let Ok(tcp) = TcpStream::connect(addres) {
            return Ok(Self {
                socket: tcp,
                connection_metadata: None,
                state: States::None,
            });
        } else {
            Err(anyhow!("Could't connect to server {:?}", addres))
        }
    }

    pub fn handshake(&mut self, user: String, password: String) -> anyhow::Result<()> {
        if let States::None = self.state {
            let mut state = States::Handshake(state::Handshake::new(user, password));
            match &mut state {
                States::Handshake(s) => {
                    s.send(&mut self.socket)?;
                    let mut packet = self.poll_packet()?;
                    while s.handle_packet(&packet)? {
                        packet = self.poll_packet()?;
                    }
                }
                _ => return Err(anyhow!("Called handshake in invalid state")),
            }
            self.state = States::ReadyToPoll(state::ReadyToPoll::new());
            Ok(())
        } else {
            Err(anyhow!("Handshake in invalid state {:?}", self.state))
        }
    }

    fn poll_packet(&mut self) -> anyhow::Result<packets::Packet> {
        let packet_size: u16 = bincode::decode_from_std_read(
            &mut self.socket,
            bincode::config::standard().with_fixed_int_encoding(),
        )?;

        let packet_id: u8 = bincode::decode_from_std_read(
            &mut self.socket,
            bincode::config::standard().with_fixed_int_encoding(),
        )?;

        if packet_id == packets::PacketId::Protocol as u8 {
            Ok(packets::Packet::Protocol(bincode::decode_from_std_read(
                &mut self.socket,
                bincode::config::standard().with_fixed_int_encoding(),
            )?))
        } else if packet_id == packets::PacketId::Welcome as u8 {
            Ok(packets::Packet::Welcome(bincode::decode_from_std_read(
                &mut self.socket,
                bincode::config::standard().with_fixed_int_encoding(),
            )?))
        } else {
            Err(anyhow!("Invalid packet"))
        }
    }

    /*
    pub fn read_packet(&mut self) -> Vec<u8> {
        let config = config::standard()
            .with_fixed_int_encoding()
            .with_little_endian();
        let mut size_buf = [0 as u8; 2];
        self.socket.read_exact(size_buf.as_mut_slice()).unwrap();
        let packet_size: u16 = bincode::decode_from_slice(size_buf.as_mut_slice(), config)
            .unwrap()
            .0;
        let mut packet_data = Vec::<u8>::new();
        packet_data.resize(packet_size as usize - 2, 0);
        println!("Got packet with size {} B", packet_size);
        self.socket.read_exact(packet_data.as_mut_slice()).unwrap();
        packet_data
    }

    pub fn handle_packet(&mut self, data: &[u8]) {
        let packet_type = data[0];
        let packet_data = &data[1..];
        println!("Got packet with type-id {}", packet_type);

        if packet_type == PacketTypesId::Welcome as u8 {
            let welcome = packets::Welcome::parse(&packet_data);
            println!("{:?}", welcome);
            self.game_info = Some(welcome);
        } else if packet_type == PacketTypesId::Protocol as u8 {
            let protocol = packets::Protocol::parse(&packet_data);
            self.protocol_params = Some(protocol);
        } else if packet_type == PacketTypesId::ServerDate as u8 {
            let date = packets::ServerDate::parse(&packet_data);
            let ymd = types::Date::new(date.date);
            println!("Today is {:?}", ymd);
        }
    }

    pub fn set_update_frequency(
        &mut self,
        update_type: packets::AdminUpdateType,
        frequency: packets::PollFrequency,
    ) -> Result<(), types::InvalidUpdate> {
        if let Some(protocol) = &self.protocol_params {
            if protocol.updates[update_type as usize].frequency & frequency as u16
                != frequency as u16
            {
                return Err(types::InvalidUpdate);
            }
        } else {
            return Err(types::InvalidUpdate);
        }
        let packet_data = packets::UpdateFrequency::new(update_type, frequency).send();
        self.socket.write(&packet_data).unwrap();
        Ok(())
    }
    pub fn handshake1(&mut self, user: &String, password: &String) {
        self.socket
            .write(packets::AdminJoin::new(user, password).send().as_slice())
            .unwrap();
        let packet_protocol = self.read_packet();
        self.handle_packet(&packet_protocol);
        let packet_welcome = self.read_packet();
        self.handle_packet(&packet_welcome);
    }

    pub fn poll_manually(&mut self, poll_type: ManualPollType, poll_id: u32) {
        self.socket
            .write(
                &packets::ManualPoll {
                    poll_id,
                    poll_type: poll_type as u8,
                }
                .send(),
            )
            .unwrap();
    }
    */
}
