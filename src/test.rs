use anyhow::{Context, Error};
use nix::sys::signal::{self, Signal};
use nix::unistd::Pid;
use std::net::{IpAddr, Ipv4Addr, SocketAddr, TcpStream};
use std::{path, process};

use super::*;

fn launch_openttd_with_conf(configuration: &str) -> anyhow::Result<process::Child> {
    let mut ttd_process = process::Command::new("openttd");
    ttd_process
        .arg("-g")
        .arg(format!("./blackbox/{}/game.sav", configuration))
        .arg("-c")
        .arg(format!("./blackbox/{}/openttd.cfg", configuration))
        .arg("-D");

    let socket = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 3977);
    let process_handle = ttd_process.spawn()?;
    for _ in (0..50) {
        if let Ok(_) = TcpStream::connect(socket) {
            return Ok(process_handle);
        }
        std::thread::sleep(std::time::Duration::from_millis(100));
    }
    Err(anyhow::anyhow!("could not open connection"))
}

fn close_openttd(ttd_handle: &mut process::Child) -> anyhow::Result<()> {
    signal::kill(Pid::from_raw(ttd_handle.id() as i32), Signal::SIGINT)?;
    ttd_handle.wait()?;
    Ok(())
}

#[test]
fn authorization() {
    let mut process = launch_openttd_with_conf("default").unwrap();

    let socket = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 3977);
    let mut ttd = connection::TTDConnection::new(socket).unwrap();
    ttd.handshake("KuczaRacza".to_owned(), "WhiteMonster".to_owned())
        .unwrap();
    close_openttd(&mut process).unwrap();
}
/*
#[test]
fn date() {
    let socket = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 3977);
    let mut ttd = connection::TTDConnection::new(socket).unwrap();
    ttd.handshake(&"KuczaRacza".to_owned(), &"mleko".to_owned());
    ttd.set_update_frequency(
        packets::AdminUpdateType::AdminUpdateDate,
        packets::PollFrequency::AdminFrequencyDaily,
    )
    .unwrap();
    loop {
        let packet = ttd.read_packet();
        ttd.handle_packet(&packet);
    }
}
#[test]
//packet per company
fn list_companies() {
    let socket = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 3977);
    let mut ttd = connection::TTDConnection::new(socket).unwrap();
    ttd.handshake(&"KuczaRacza".to_owned(), &"mleko".to_owned());
    ttd.poll_manually(connection::ManualPollType::CompanyInfo, u32::MAX);
    loop {
        let packet = ttd.read_packet();
        ttd.handle_packet(&packet);
    }
}
 */
