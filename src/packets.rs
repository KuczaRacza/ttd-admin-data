use struct_iterable::Iterable;

use crate::types::NullTerminated;
use bincode::config;
use bincode::Decode;
use bincode::Encode;

pub enum PacketId {
    Join,
    Protocol = 103,
    Welcome = 104,
    ServerDate = 107,
    UpdateFrequency = 2,
    ManualPoll = 3,
}
#[derive(Debug)]
pub enum Packet {
    AdminJoin(AdminJoin),
    Protocol(Protocol),
    Welcome(Welcome),
}

const BYTES_PER_PACKET_TYPE: usize = 1;
const BYTES_PER_PACKET_SIZE: usize = 2;
#[allow(dead_code)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum PollFrequency {
    AdminFrequencyPoll = 0x01,      //< The admin can poll this.
    AdminFrequencyDaily = 0x02,     //< The admin gets information about this on a daily basis.
    AdminFrequencyWeekly = 0x04,    //< The admin gets information about this on a weekly basis.
    AdminFrequencyMonthly = 0x08,   //< The admin gets information about this on a monthly basis.
    AdminFrequencyQuarterly = 0x10, //< The admin gets information about this on a quarterly basis.
    AdminFrequencyAnually = 0x20,   //< The admin gets information about this on a yearly basis.
    AdminFrequencyAutomatic = 0x40, //< The admin gets information about this when it changes.
}
#[allow(dead_code)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum AdminUpdateTypeId {
    AdminUpdateDate,           //< Updates about the date of the game.
    AdminUpdateClientInfo,     //< Updates about the information of clients.
    AdminUpdateCompanyInfo,    //< Updates about the generic information of companies.
    AdminUpdateCompanyEconomy, //< Updates about the economy of companies.
    AdminUpdateCompanyStats,   //< Updates about the statistics of companies.
    AdminUpdateChat,           //< The admin would like to have chat messages.
    AdminUpdateConsole,        //< The admin would like to have console messages.
    AdminUpdateCmdNames,       //< The admin would like a list of all DoCommand names.
    AdminUpdateCmdLogging,     //< The admin would like to have DoCommand information.
    AdminUpdateGamescript,     //< The admin would like to have gamescript messages.
    AdminUpdateEnd,            //< Must ALWAYS be on the end of this list!! (period)
}

#[derive(Debug, Clone, Copy)]
pub struct ServerDate {
    pub date: u32,
}
impl Decode for ServerDate {
    fn decode<D: bincode::de::Decoder>(
        decoder: &mut D,
    ) -> Result<Self, bincode::error::DecodeError> {
        Ok(Self {
            date: Decode::decode(decoder).unwrap(),
        })
    }
}

#[derive(Debug, Clone, Copy)]
pub struct AdminUpdateTypeState {
    pub some_bool: u8, //should be always true. idk what it's
    pub id: u16,       //0-9
    pub frequency: u16,
}
impl Decode for AdminUpdateTypeState {
    fn decode<D: bincode::de::Decoder>(
        decoder: &mut D,
    ) -> Result<Self, bincode::error::DecodeError> {
        Ok(Self {
            some_bool: Decode::decode(decoder).unwrap(),
            id: Decode::decode(decoder).unwrap(),
            frequency: Decode::decode(decoder).unwrap(),
        })
    }
}
#[derive(Debug)]
pub struct Protocol {
    pub protocol_version: u8,
    pub updates: [AdminUpdateTypeState; 10],
    pub some_bool: u8, // always false, idk what doing
}
impl Decode for Protocol {
    fn decode<D: bincode::de::Decoder>(
        decoder: &mut D,
    ) -> Result<Self, bincode::error::DecodeError> {
        Ok(Self {
            protocol_version: Decode::decode(decoder)?,
            updates: Decode::decode(decoder)?,
            some_bool: Decode::decode(decoder)?,
        })
    }
}

#[derive(Clone, Debug)]
pub struct Welcome {
    pub name: NullTerminated,
    pub revision: NullTerminated,
    pub is_dedicated: u8,
    pub depricated_map_name: NullTerminated,
    pub seed: u32,
    pub is_landscape_mod: u8,
    pub date: u32,
    pub size_x: u16,
    pub size_y: u16,
}

impl Decode for Welcome {
    fn decode<D: bincode::de::Decoder>(
        decoder: &mut D,
    ) -> Result<Self, bincode::error::DecodeError> {
        Ok(Self {
            name: Decode::decode(decoder).unwrap(),
            revision: Decode::decode(decoder).unwrap(),
            is_dedicated: Decode::decode(decoder).unwrap(),
            depricated_map_name: Decode::decode(decoder).unwrap(),
            seed: Decode::decode(decoder).unwrap(),
            is_landscape_mod: Decode::decode(decoder).unwrap(),
            date: Decode::decode(decoder).unwrap(),
            size_x: Decode::decode(decoder).unwrap(),
            size_y: Decode::decode(decoder).unwrap(),
        })
    }
}

#[derive(Debug)]
pub struct AdminJoin {
    pub password: NullTerminated,
    pub user: NullTerminated,
    pub revision: NullTerminated,
}
impl AdminJoin {
    pub fn new(user: &str, password: &str) -> Self {
        Self {
            password: password.as_bytes().into(),
            user: user.as_bytes().into(),
            revision: "ttd-admin-data 0.1.0 KuczaRacza.com".as_bytes().into(),
        }
    }
}
impl Encode for AdminJoin {
    fn encode<E: bincode::enc::Encoder>(
        &self,
        encoder: &mut E,
    ) -> Result<(), bincode::error::EncodeError> {
        let size = (self.password.enc_len()
            + self.user.enc_len()
            + self.revision.enc_len()
            + BYTES_PER_PACKET_TYPE
            + BYTES_PER_PACKET_SIZE) as u16;
        bincode::Encode::encode(&size, encoder)?;
        bincode::Encode::encode(&(PacketId::Join as u8), encoder)?;
        bincode::Encode::encode(&self.password, encoder)?;
        bincode::Encode::encode(&self.user, encoder)?;
        bincode::Encode::encode(&self.revision, encoder)?;
        Ok(())
    }
}

#[derive(Debug, Clone, Copy)]
pub struct UpdateFrequency {
    pub update_type: u16,
    pub frequency: u16,
}
impl UpdateFrequency {
    pub fn new(update_type: AdminUpdateTypeId, frequency: PollFrequency) -> Self {
        Self {
            update_type: update_type as u16,
            frequency: frequency as u16,
        }
    }
}
impl Encode for UpdateFrequency {
    fn encode<E: bincode::enc::Encoder>(
        &self,
        encoder: &mut E,
    ) -> Result<(), bincode::error::EncodeError> {
        let size = (BYTES_PER_PACKET_SIZE
            + BYTES_PER_PACKET_TYPE
            + std::mem::size_of::<u16>()
            + std::mem::size_of::<u16>()) as u16;
        Encode::encode(&size, encoder)?;
        Encode::encode(&(PacketId::UpdateFrequency as u8), encoder)?;
        Encode::encode(&self.update_type, encoder)?;
        Encode::encode(&self.frequency, encoder)?;
        Ok(())
    }
}

pub struct ManualPoll {
    pub poll_type: u8,
    pub poll_id: u32,
}
impl Encode for ManualPoll {
    fn encode<E: bincode::enc::Encoder>(
        &self,
        encoder: &mut E,
    ) -> Result<(), bincode::error::EncodeError> {
        let size: u16 = (BYTES_PER_PACKET_TYPE
            + BYTES_PER_PACKET_SIZE
            + std::mem::size_of::<u8>()
            + std::mem::size_of::<u32>()) as u16;
        Encode::encode(&size, encoder)?;
        Encode::encode(&(PacketId::ManualPoll as u8), encoder)?;
        Encode::encode(&self.poll_type, encoder)?;
        Encode::encode(&self.poll_id, encoder)?;
        Ok(())
    }
}
