use std::fmt::Debug;
use std::fmt::Write;
use std::string::FromUtf8Error;

use bincode::Decode;
use bincode::Encode;
#[derive(Clone, Default)]
pub struct NullTerminated {
    pub data: Vec<u8>,
}
impl From<Vec<u8>> for NullTerminated {
    fn from(value: Vec<u8>) -> Self {
        Self { data: value }
    }
}
impl From<&[u8]> for NullTerminated {
    fn from(value: &[u8]) -> Self {
        Self { data: value.into() }
    }
}
impl Debug for NullTerminated {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = self.data.iter().map(|c| *c as char).collect::<String>();
        f.write_str(&str)
    }
}
impl Decode for NullTerminated {
    fn decode<D: bincode::de::Decoder>(
        decoder: &mut D,
    ) -> Result<Self, bincode::error::DecodeError> {
        let mut vec = Vec::<u8>::new();
        let mut last_byte: u8;
        loop {
            last_byte = bincode::Decode::decode(decoder)?;
            if last_byte == 0 {
                return Ok(NullTerminated::from(vec));
            }
            vec.push(last_byte);
        }
    }
}
impl Encode for NullTerminated {
    fn encode<E: bincode::enc::Encoder>(
        &self,
        encoder: &mut E,
    ) -> Result<(), bincode::error::EncodeError> {
        for byte in &self.data {
            bincode::Encode::encode(&byte, encoder)?;
        }
        bincode::Encode::encode(&(0 as u8), encoder)?;
        Ok(())
    }
}
impl NullTerminated {
    pub fn enc_len(&self) -> usize {
        self.data.len() + 1
    }
}

impl  TryInto<String> for NullTerminated {
    type Error = FromUtf8Error;
    fn try_into(self) -> Result<String, Self::Error> {
        String::from_utf8(self.data)
    }
}

pub struct InvalidUpdate;
impl Debug for InvalidUpdate {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("invalid update request, unsupported update frequency for this type or socket is not connected")
    }
}
#[derive(Clone, Copy, Debug)]
pub struct Date {
    pub year: u32,
    pub month: u32,
    pub day: u32,
}
const DAYS_IN_MONTH: [u32; 12] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const DAYS_IN_YEAR: u32 = 365 as u32;

impl Date {
    pub fn is_leap_year(yr: u32) -> bool {
        yr % 4 == 0 && (yr % 100 != 0 || yr % 400 == 0)
    }
    pub fn new(date: u32) -> Self {
        let mut yr = 400 * (date / (DAYS_IN_YEAR * 400 + 97));
        let mut rem = date % (DAYS_IN_YEAR * 400 + 97);
        if rem >= DAYS_IN_YEAR * 100 + 25 {
            yr += 100;
            rem -= DAYS_IN_YEAR * 100 + 25;
            yr += 100 * (rem / (DAYS_IN_YEAR * 100 + 24));
            rem = rem % (DAYS_IN_YEAR * 100 + 24);
        }
        if Date::is_leap_year(yr) && rem >= DAYS_IN_YEAR * 4 {
            yr += 4;
            rem -= DAYS_IN_YEAR * 4;
        }
        yr += 4 * (rem / (DAYS_IN_YEAR * 4 + 1));
        rem = rem % (DAYS_IN_YEAR * 4 + 1);
        while rem >= DAYS_IN_YEAR + Date::is_leap_year(yr) as u32 {
            rem = rem - (DAYS_IN_YEAR + Date::is_leap_year(yr) as u32);
            yr += 1;
        }
        let mut month = 0 as u32;
        loop {
            let days_in_next_mont =
                DAYS_IN_MONTH[month as usize] + (Date::is_leap_year(yr) && month == 1) as u32;
            if days_in_next_mont >= rem {
                break;
            }
            rem -= days_in_next_mont;
            month += 1;
        }
        if Date::is_leap_year(yr) {}
        Self {
            year: yr,
            month: month,
            day: rem,
        }
    }
}
